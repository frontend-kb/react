[[_TOC_]]

## Detect infinite re-rendering loops

Get [stop-runaway-react-effects](https://github.com/kentcdodds/stop-runaway-react-effects) into your project

**tl;dr**

The problem:

`useEffect` or `useLayoutEffect` may lead to infinite loop triggering re-renders to death.

The solution:

This module wraps `useEffect` and `useLayoutEffect` to detect such infinite loops and warns you about it in browser console