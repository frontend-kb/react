[[_TOC_]]

## 'React' refers to a UMD global, but the current file is a module
See [StackOverflow](https://stackoverflow.com/a/65539274)

**Best way**

Requirements:
- typescript > 4.1
- react/react-dom > 17

Solution:

`tsconfig.json`
```json
{
    "compilerOptions": {
        "jsx": "react-jsx"
    }
}
```

**Otherwise**

```javascript
import React from 'react'
```

