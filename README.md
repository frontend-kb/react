[[_TOC_]]

## [Improve quality of code  :link:](./CODE-QUALITY.md)

## [Patterns :link:](./PATTERNS.md)

## [Troubleshooting :link:](./TROUBLESHOOTING.md)

