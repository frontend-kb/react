[[_TOC_]]

## Compound Components

### The idea

Compound components injects props despite passing those in JSX to each component

```jsx
function App() {
    return (
        <Main>
            <Value /> is <ValueType />
        </Main>
    )
}
```

Both `Value` and `ValueType` takes `value` prop

```jsx
// takes the value prop
function ValueType({value}) {
    return <span>{typeof value}</span>
}

// takes the value prop
function Value({value}) {
    if (typeof value !== 'object')
        return <span>{value}</span>
    else
        const stringified = JSON.stringify(value)
        return <span>{stringified}</span>
}
```

The responsibility of providing value is on `Main` component

### Basic, immediate children (JS only)

```jsx
function Main({children}) {
    // stores some state
    const [value, setValue] = useState()

    // injects the state to children automatically
    return React.Children.map(children, (child) => {
        if (typeof child.type === 'string')
            return child

        return React.cloneElement(child, {value})
    })
}
```

### Selective injecting, immediate children (JS only)

You want to pass props to only subset of children. Let's change the `App` slightly

```jsx
// takes the value prop
function AnotherComponent({value}) {
    return !!value ? `The value is ${value}` : 'No value provided'
}

const allowedTypes = [Value, ValueType]

function App() {
    return (
        <Main>
            {/* these should receive the `value` prop */}
            <Value /> is <ValueType />

            {/* this shouldn't */}
            <AnotherComponent />
            {/* and render 'No value provided' by default */}
        </Main>
    )
}
```

Then, the props provider will look like:

```jsx
function Main({children}) {
    // stores some state
    const [value, setValue] = useState()

    // injects the state to children automatically
    return React.Children.map(children, (child) => {
        if (!allowedTypes.includes(child.type))
            return child

        return React.cloneElement(child, {value})
    })
}
```
